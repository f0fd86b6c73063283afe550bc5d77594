#include <cstdint>
#include <iostream>
#include <vector>

#include "Base64.h"

int test(const std::string &data) {
  std::string out;
  auto b64 = macaron::Base64::Encode(data);
  auto error = macaron::Base64::Decode(b64, out);
  if (!error.empty()) {
    std::cout << "Error: " << error << std::endl;
    return 1;
  }
  if (data == out) {
    std::cout << "OK: " << out << std::endl;
  } else {
    std::cout << "Wrong: " << data << ", " << b64 << ", " << out << std::endl;
  }
  return 0;
}

int main() {
  test("hello");
  test("");
  test("1");
  test("22");
  test("333");
  test("4444");
  return 0;
}
